import Data.List
import Debug.Trace
import Text.PrettyPrint

data Puzzle = Puzzle [[Int]] [[Int]]

data Solution = 
  Solution [Possibilities] [Possibilities]  |
  Unsolvable
  deriving (Eq)

instance Show Solution where
  show Unsolvable = "Unsolvable"
  show (Solution rows cols) = concat $ map (\ps -> "\n" ++ (concat $ map show $ foldl1 unionPattern ps)) rows

type Pattern = [Cell]
type Possibilities = [Pattern]

data Cell = Black | White | Either deriving (Eq)
instance Show Cell where
  show White = "  "
  show Black = "[]"
  show Either = ". "

solve :: Solution -> Solution
solve s = 
  let s' = refine s in
  if solved s' then s' else 
    trace "guessing" $ 
    let as = [Solution rs cs | (Solution rs cs) <- map solve (alternatives s')] in
    case as of
      [] -> Unsolvable
      s -> head s

alternatives :: Solution -> [Solution]
alternatives Unsolvable = []
alternatives (Solution rs cs) = 
  let mi = findIndex (\r -> length r > 1) rs in
  case mi of 
    Nothing -> [] 
    Just i -> [Solution (setIndex rs i r) cs | r <- split' (rs!!i)]

split :: [a] -> [[a]]
split [] = [[], []]
split [a] = [[a],[]]
split (a:b:cs) = [(a:as), (b:bs)] where [as,bs] = split cs

-- This kind of splitting is much slower for puzzle041 but faster for tough
-- Probably just chance
split' :: [a] -> [[a]]
split' as = [take l2 as, drop l2 as] where l2 = length as `div` 2

setIndex :: [a] -> Int -> a -> [a]
setIndex as i a = take i as ++ [a] ++ drop (i + 1) as

solved :: Solution -> Bool
solved Unsolvable = False
solved (Solution rs cs) = (findIndex (\r -> length r > 1) rs) == Nothing

unsolved :: Puzzle -> Solution
unsolved (Puzzle rows cols) = Solution [possiblePatterns (length cols) r | r <- rows] 
                                       [possiblePatterns (length rows) c | c <- cols]

refine :: Solution -> Solution
refine Unsolvable = Unsolvable
refine (Solution rs cs) = 
  let rs' = filterIncompatible rs cs in
  if 0 == (minimum $ map length rs') then Unsolvable else
  let cs' = filterIncompatible cs rs' in
  if 0 == (minimum $ map length cs') then Unsolvable else
  if (rs', cs') == (rs, cs) 
    then Solution rs cs
    else trace (show $ Solution rs cs) $ refine $ Solution rs' cs'
  
filterIncompatible :: [Possibilities] -> [Possibilities] -> [Possibilities]
filterIncompatible rs cs = -- could be rows,cols or cols,rows
  let checks = transpose $ map (foldl1 unionPattern) cs in
  map (\ (ps, c) -> filter (consistentPattern c) ps) (zip rs checks)

consistentPattern :: Pattern -> Pattern -> Bool
consistentPattern p p' = and [consistentCell c c' | (c, c') <- zip p p']

consistentCell :: Cell -> Cell -> Bool
consistentCell c c' = c == Either || c' == Either || c == c'

cellUnion :: Cell -> Cell -> Cell
cellUnion c c' = if c == c' then c else Either

unionPattern :: Pattern -> Pattern -> Pattern
unionPattern r r' = zipWith cellUnion r r'

possiblePatterns :: Int -> [Int] -> Possibilities
possiblePatterns total [] = [replicate total White]
possiblePatterns total [n] = 
  [(replicate i White) ++ (replicate n Black) ++ (replicate (total - i - n) White) |
    i <- [0..total-n]]
possiblePatterns total (n:ns) = [(replicate i White) ++ (replicate n Black) ++ [White] ++ ps |
  i <- [0..total - n - (sum ns) - (length ns)],
  ps <- possiblePatterns (total - i - n - 1) ns]

ez = Puzzle [[3],[2],[1]] [[3],[2],[1]]

vague = Puzzle [[1], [1]] [[1], [1]]

impossible = Puzzle [[1]] [[]]

moose = Puzzle 
  [[2,2,2,2],[2,2,1,1,2,2],[4,1,1,2,2],[5,5],[2,5],[7],[1,1,5],[11],[12],[14],[8,6],[1,3,6],[5,6],[3,6]]
  [[2],[3,3],[2,2,2],[4,6],[5,7],[2,7],[3,1,4],[7],[2,4],[2,6],[11],[14],[4,9],[1,8],[4,7],[3,5]]

puzzle042 = Puzzle
  [[1,1],[1,2],[6],[1,3],[2,3],                       [1,3,2],[2,1,3],[3,1,3],[8,2,2],[5,4,2],
   [2,1,1],[2,1,1,1],[3,1,1,1,2],[3,2,1,2,3],[2,6,4], [1,1,3,2],[4,2,1],[2,2,1,1],[2,2,2,1],[2,2,1]]
  [[1],[2],[6],[6],[3],                               [7],[2,2,2],[1,5],[1,1,2],[2,4,1],
   [3,1,3,2],[3,1,1,1,2],[3,1,2,2,1],[1,4,4],[3,1,2], [5,2],[6,7],[11,2],[4,3],[5]]

-- These two are from http://www.pro.or.jp/~fuji/java/puzzle/nonogram/index-eng.html
-- They're 10x10 and require guessing to complete
tough1 = Puzzle
  [[3],[2,1],[1,1],[1,4],[1,1,1,1],[2,1,1,1],[2,1,1],[1,2],[2,3],[3]]
  [[3],[2,1],[2,2],[2,1],[1,2,1],[1,1],[1,4,1],[1,1,2],[3,1],[4]]

tough2 = Puzzle
  [[1],[3],[1,3],[2,4],[1,2],[2,1,1],[1,1,1,1],[2,1,1],[2,2],[5]]
  [[4],[1,3],[2,3],[1,2],[2,2],[1,1,1],[1,1,1,1],[1,1,1],[1,2],[5]]

main = print $ solve $ unsolved tough2
